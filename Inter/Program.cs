﻿using Inter;
using Inter.Entities;
using Inter.ExcepptionHandllers;
using Inter.IServices;
using Inter.Services;

class Program
{
    static void Main(string[] args)
    {
        using (var db = new AppDbContext())
        {

            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddTransient<ExceptionMiddleware>();

            builder.Services.AddScoped<IKieuThanhVien, KieuThanhVienService>();
            builder.Services.AddScoped<IChua, ChuaService>();
            builder.Services.AddSingleton<IFile, FileService>();
            builder.Services.AddScoped<IPhatTu, PhatTuService>();

            var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
            // Add services to the container.
            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddControllers().AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            builder.Services.AddCors(x => x.AddPolicy(MyAllowSpecificOrigins, builder =>
            {
                builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
            }));

            var app = builder.Build();

            app.UseMiddleware<ExceptionMiddleware>();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
                app.UseCors(MyAllowSpecificOrigins);
            }

            app.UseRouting();

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();
            DatabaseFirst.CreateDatabase(db);

            app.Run();
        }
    }
}