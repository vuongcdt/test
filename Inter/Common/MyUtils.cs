﻿using Microsoft.IdentityModel.Tokens;

namespace Inter.Common
{
    public class MyUtils
    {
        public static string LocDau(string str)
        {
            if (str.IsNullOrEmpty()) return null;

            string[] VietNamChar = new string[]
              {
                    "aeiouyd",
                    "àáạảãâầấậẩẫăằắặẳẵ",
                    "èéẹẻẽêềếệểễ",
                    "ìíịỉĩ",
                    "òóọỏõôồốộổỗơờớợởỡ",
                    "ùúụủũưừứựửữ",
                    "ỳýỵỷỹ",
                    "đ"
              };
            str = str.ToLower();
            for (int i = 1; i < VietNamChar.Length; i++)
            {
                for (int j = 0; j < VietNamChar[i].Length; j++)
                    str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
            }
            return str;
        }
    }
}
