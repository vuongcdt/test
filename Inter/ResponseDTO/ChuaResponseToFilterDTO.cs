﻿using Inter.Entities;

namespace Inter.ResponseDTO
{
    public class ChuaResponseToFilterDTO
    {
        public int Id { get; set; }
        public string TenChua { get; set; }

        public static ChuaResponseToFilterDTO Mapping(Chua chua)
        {
            return new ChuaResponseToFilterDTO
            {
                Id = chua.Id,
                TenChua = chua.TenChua
            };
        }
    }
}
