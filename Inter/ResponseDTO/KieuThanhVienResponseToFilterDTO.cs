﻿using Inter.Entities;

namespace Inter.ResponseDTO
{
    public class KieuThanhVienResponseToFilterDTO
    {
        public int Id { get; set; }
        public string TenKieu { get; set; }

        public static KieuThanhVienResponseToFilterDTO Mapping(KieuThanhVien kieuThanhVien)
        {
            return new KieuThanhVienResponseToFilterDTO
            {
                Id = kieuThanhVien.Id,
                TenKieu = kieuThanhVien.TenKieu,
            };
        }
    }
}
