﻿namespace Inter.Entities
{
    public class PhatTu
    {
        public int Id { get; set; }
        public string Ho { get; set; }
        public string TenDem { get; set; }
        public string Ten { get; set; }
        public string PhapDanh { get; set; }
        public string AnhChup { get; set; }
        public string SoDienThoai { get; set; }
        public string Email { get; set; }
        public DateTime NgaySinh { get; set; }
        public DateTime NgayXuatGia { get; set; }
        public bool DaHoanTuc { get; set; }
        public DateTime NgayHoanTuc { get; set; }
        public int GioiTinh { get; set; }
        public int KieuThanhVienId { get; set; }
        public KieuThanhVien? KieuThanhVien { get; set; }
        public DateTime NgayCapNhat { get; set; }
        public int ChuaId { get; set; }
        public Chua? Chua { get; set; }
        public string? PhapDanhUnsigned { get; set; }
    }
}
