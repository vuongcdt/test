﻿namespace Inter.Entities
{
    public class DaoTrang
    {
        public int Id { get; set; }
        public string NoiToChuc { get; set; }
        public int SoThanhVienThamGia { get; set; }
        public int NguoiTruTriId { get; set; }
        public DateTime ThoiGianToChuc { get; set; }
        public string NoiDung { get; set; }
        public bool DaKetThuc { get; set; }

    }
}
