﻿namespace Inter.Entities
{
    public class KieuThanhVien
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string TenKieu { get; set; }
    }
}
