﻿using Microsoft.EntityFrameworkCore;

namespace Inter.Entities
{
    public class AppDbContext : DbContext
    {
        public virtual DbSet<PhatTu> PhatTus { get; set; }
        public virtual DbSet<DonDangKy> DonDangKys { get; set; }
        public virtual DbSet<KieuThanhVien> KieuThanhViens { get; set; }
        public virtual DbSet<PhatTuDaoTrang> PhatTuDaoTrangs { get; set; }
        public virtual DbSet<DaoTrang> DaoTrangs { get; set; }
        public virtual DbSet<Chua> Chuas { get; set; }

        public static readonly ILoggerFactory factory = LoggerFactory.Create(builder => builder.AddConsole());

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = $"Server = DESKTOP-RJDTR74\\SQLEXPRESS ; Database = QLPhatTu ; Trusted_Connection = true ; TrustServerCertificate=True";
            optionsBuilder
                .UseSqlServer(connectionString)
                .UseLoggerFactory(factory)
            ;

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<PhatTu>().Property(e => e.PhapDanh).UseCollation("SQL_Latin1_General_CP1_CI_AS");
            modelBuilder.Entity<PhatTu>(e =>
            {
                e.HasMany<DonDangKy>().WithOne().HasForeignKey(x => x.PhatTuId).IsRequired(false);
                e.HasMany<DonDangKy>().WithOne().HasForeignKey(x => x.NguoiXuLyId);
                e.HasMany<PhatTuDaoTrang>().WithOne().HasForeignKey(x => x.PhatTuId).IsRequired(false);
                e.HasMany<DaoTrang>().WithOne().HasForeignKey(x => x.NguoiTruTriId);
                e.HasOne(e => e.Chua).WithMany().HasForeignKey(x => x.ChuaId);
                e.HasOne(e => e.KieuThanhVien).WithMany().HasForeignKey(x => x.KieuThanhVienId);
            });

            modelBuilder.Entity<DaoTrang>(e =>
            {
                e.HasMany<PhatTuDaoTrang>().WithOne().HasForeignKey(x => x.DaoTrangId).IsRequired(true);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
