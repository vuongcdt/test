﻿using Inter.Entities;
using Inter.RequestDTO;
using Inter.ResponseDTO;

namespace Inter.IServices
{
    public interface IPhatTu
    {
        PhatTuServiceDTO GetAll(PhatTuRequestParams phatTuParams);
        ResponseMessage Create(PhatTu phatTu);
        ResponseMessage Update(PhatTu phatTu);
        ResponseMessage Delete(int id);
    }
}
