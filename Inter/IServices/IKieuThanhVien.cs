﻿using Inter.ResponseDTO;

namespace Inter.IServices
{
    public interface IKieuThanhVien
    {
        List<KieuThanhVienResponseToFilterDTO> GetAllToFilter();

    }
}
