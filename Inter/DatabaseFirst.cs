﻿using Bogus;
using Inter.Common;
using Inter.Entities;
using Microsoft.IdentityModel.Tokens;

namespace Inter
{
    public class DatabaseFirst
    {
        public static async void CreateDatabase(AppDbContext db)
        {
            if (!db.KieuThanhViens.IsNullOrEmpty()) return;
            using (var trans = db.Database.BeginTransaction())
            {
                var isBool = new[] { true, false };
                var gender = new[] { 1, 0 };
                var trangThaiDonArray = new[] { 0, 1 };
                var kieuThanhVienArray = new[] { "Trụ trì", "Hòa thượng" };

                var faker = new Faker("vi");
                Func<DateTime> ramdomDate = () => faker.Date.Between(DateTime.Parse("2010-01-01"), DateTime.Parse("2023-05-31"));
                Func<int, string> lorem = (limit) => new Bogus.DataSets.Lorem(locale: "vi").Sentence(limit);

                Console.WriteLine("Add database KieuThanhVien Chua");

                db.KieuThanhViens.Add(new KieuThanhVien { TenKieu = "Trụ trì", Code = "TT" });
                db.KieuThanhViens.Add(new KieuThanhVien { TenKieu = "Hòa thượng", Code = "HT" });

                Faker<Chua> fakerChua = new Faker<Chua>("vi")
                    .RuleFor(u => u.TenChua, f => f.Name.FirstName())
                    .RuleFor(u => u.NgayThanhLap, f => ramdomDate())
                    .RuleFor(u => u.DiaChi, f => f.Address.FullAddress())
                    .RuleFor(u => u.TruTri, f => f.Name.FullName())
                    .RuleFor(u => u.CapNhat, f => ramdomDate())
                    ;
                db.Chuas.AddRange(fakerChua.Generate(10));

                await db.SaveChangesAsync();


                if (db.PhatTus.Count() < 10)
                {
                    Console.WriteLine("Add database PhatTus");

                    List<int> chuaIdList = db.Chuas.Select(e => e.Id).ToList();
                    List<int> kieuThanhVienIdList = db.KieuThanhViens.Select(e => e.Id).ToList();

                    Faker<PhatTu> fakerPhatTu = new Faker<PhatTu>("vi")
                            .RuleFor(u => u.AnhChup, f => f.Internet.Avatar())
                            .RuleFor(u => u.Ho, f => f.Name.LastName())
                            .RuleFor(u => u.Ten, f => f.Name.FirstName())
                            .RuleFor(u => u.TenDem, f => f.Name.LastName())
                            .RuleFor(u => u.PhapDanh, (f, u) => "Thích " + f.Name.FirstName())
                            .RuleFor(u => u.PhapDanhUnsigned, (f, u) => MyUtils.LocDau(u.PhapDanh))
                            .RuleFor(u => u.SoDienThoai, f => f.Random.Replace("09########"))
                            .RuleFor(u => u.Email, f => f.Internet.Email())
                            .RuleFor(u => u.DaHoanTuc, f => f.PickRandom(isBool))
                            .RuleFor(u => u.GioiTinh, f => f.PickRandom(gender))
                            .RuleFor(u => u.KieuThanhVienId, f => f.PickRandom(kieuThanhVienIdList))
                            .RuleFor(u => u.ChuaId, f => f.PickRandom(chuaIdList))
                            .RuleFor(u => u.NgaySinh, f => ramdomDate())
                            .RuleFor(u => u.NgayXuatGia, f => ramdomDate())
                            .RuleFor(u => u.NgayHoanTuc, f => ramdomDate())
                            .RuleFor(u => u.NgayCapNhat, f => ramdomDate());
                    db.PhatTus.AddRange(fakerPhatTu.Generate(50));
                    await db.SaveChangesAsync();

                    List<int> phatTuIdListDb = db.PhatTus.Select(e => e.Id).ToList();

                    Console.WriteLine("Add database DonDangKy");

                    Faker<DonDangKy> fakerDonDangKy = new Faker<DonDangKy>("vi")
                        .RuleFor(u => u.PhatTuId, f => f.PickRandom(phatTuIdListDb))
                        .RuleFor(u => u.NgayGuiDon, f => ramdomDate())
                        .RuleFor(u => u.NgayXuLy, f => ramdomDate())
                        .RuleFor(u => u.NguoiXuLyId, f => f.PickRandom(phatTuIdListDb))
                        .RuleFor(u => u.TrangThaiDon, f => f.PickRandom(trangThaiDonArray))
                    ;

                    List<DonDangKy> donDangKyList = fakerDonDangKy.Generate(50);
                    db.DonDangKys.AddRange(donDangKyList);
                    await db.SaveChangesAsync();

                    Console.WriteLine("Add database DaoTrangs");

                    Faker<DaoTrang> fakerDaoTrang = new Faker<DaoTrang>("vi")
                        .RuleFor(u => u.DaKetThuc, f => f.PickRandom(isBool))
                        .RuleFor(u => u.NoiDung, f => f.Lorem.Sentence(20))
                        .RuleFor(u => u.NoiToChuc, f => f.Address.FullAddress())
                        .RuleFor(u => u.SoThanhVienThamGia, f => 0)
                        .RuleFor(u => u.ThoiGianToChuc, f => ramdomDate())
                        .RuleFor(u => u.NguoiTruTriId, f => f.PickRandom(phatTuIdListDb))
                    ;
                    db.DaoTrangs.AddRange(fakerDaoTrang.Generate(20));
                    await db.SaveChangesAsync();

                    Console.WriteLine("Add database PhatTuDaoTrang");

                    List<int> daoTrangIdList = db.DaoTrangs.Select(e => e.Id).ToList();

                    Faker<PhatTuDaoTrang> fakerPhatTuDaoTrang = new Faker<PhatTuDaoTrang>("vi")
                        .RuleFor(u => u.PhatTuId, f => f.PickRandom(phatTuIdListDb))
                        .RuleFor(u => u.DaoTrangId, f => f.PickRandom(daoTrangIdList))
                        .RuleFor(u => u.DaThamGia, f => f.PickRandom(isBool))
                        .RuleFor(u => u.LyDoKhongThamGia, f => f.Lorem.Sentence(20))
                    ;
                    db.PhatTuDaoTrangs.AddRange(fakerPhatTuDaoTrang.Generate(20));
                    await db.SaveChangesAsync();
                }
                trans.Commit();
                await Console.Out.WriteLineAsync("Done add first database !!!");
            }

        }
    }
}
