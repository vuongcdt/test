﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Inter.Migrations
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Chuas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenChua = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NgayThanhLap = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DiaChi = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TruTri = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CapNhat = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chuas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KieuThanhViens",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TenKieu = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KieuThanhViens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PhatTus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ho = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TenDem = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Ten = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PhapDanh = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AnhChup = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SoDienThoai = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NgaySinh = table.Column<DateTime>(type: "datetime2", nullable: false),
                    NgayXuatGia = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DaHoanTuc = table.Column<bool>(type: "bit", nullable: false),
                    NgayHoanTuc = table.Column<DateTime>(type: "datetime2", nullable: false),
                    GioiTinh = table.Column<int>(type: "int", nullable: false),
                    KieuThanhVienId = table.Column<int>(type: "int", nullable: false),
                    NgayCapNhat = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ChuaId = table.Column<int>(type: "int", nullable: false),
                    PhapDanhUnsigned = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhatTus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhatTus_Chuas_ChuaId",
                        column: x => x.ChuaId,
                        principalTable: "Chuas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PhatTus_KieuThanhViens_KieuThanhVienId",
                        column: x => x.KieuThanhVienId,
                        principalTable: "KieuThanhViens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DaoTrangs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NoiToChuc = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SoThanhVienThamGia = table.Column<int>(type: "int", nullable: false),
                    NguoiTruTriId = table.Column<int>(type: "int", nullable: false),
                    ThoiGianToChuc = table.Column<DateTime>(type: "datetime2", nullable: false),
                    NoiDung = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DaKetThuc = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DaoTrangs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DaoTrangs_PhatTus_NguoiTruTriId",
                        column: x => x.NguoiTruTriId,
                        principalTable: "PhatTus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DonDangKys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PhatTuId = table.Column<int>(type: "int", nullable: false),
                    TrangThaiDon = table.Column<int>(type: "int", nullable: false),
                    NgayGuiDon = table.Column<DateTime>(type: "datetime2", nullable: false),
                    NgayXuLy = table.Column<DateTime>(type: "datetime2", nullable: false),
                    NguoiXuLyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DonDangKys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DonDangKys_PhatTus_NguoiXuLyId",
                        column: x => x.NguoiXuLyId,
                        principalTable: "PhatTus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DonDangKys_PhatTus_PhatTuId",
                        column: x => x.PhatTuId,
                        principalTable: "PhatTus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PhatTuDaoTrangs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PhatTuId = table.Column<int>(type: "int", nullable: true),
                    DaoTrangId = table.Column<int>(type: "int", nullable: false),
                    DaThamGia = table.Column<bool>(type: "bit", nullable: false),
                    LyDoKhongThamGia = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhatTuDaoTrangs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhatTuDaoTrangs_DaoTrangs_DaoTrangId",
                        column: x => x.DaoTrangId,
                        principalTable: "DaoTrangs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PhatTuDaoTrangs_PhatTus_PhatTuId",
                        column: x => x.PhatTuId,
                        principalTable: "PhatTus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_DaoTrangs_NguoiTruTriId",
                table: "DaoTrangs",
                column: "NguoiTruTriId");

            migrationBuilder.CreateIndex(
                name: "IX_DonDangKys_NguoiXuLyId",
                table: "DonDangKys",
                column: "NguoiXuLyId");

            migrationBuilder.CreateIndex(
                name: "IX_DonDangKys_PhatTuId",
                table: "DonDangKys",
                column: "PhatTuId");

            migrationBuilder.CreateIndex(
                name: "IX_PhatTuDaoTrangs_DaoTrangId",
                table: "PhatTuDaoTrangs",
                column: "DaoTrangId");

            migrationBuilder.CreateIndex(
                name: "IX_PhatTuDaoTrangs_PhatTuId",
                table: "PhatTuDaoTrangs",
                column: "PhatTuId");

            migrationBuilder.CreateIndex(
                name: "IX_PhatTus_ChuaId",
                table: "PhatTus",
                column: "ChuaId");

            migrationBuilder.CreateIndex(
                name: "IX_PhatTus_KieuThanhVienId",
                table: "PhatTus",
                column: "KieuThanhVienId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DonDangKys");

            migrationBuilder.DropTable(
                name: "PhatTuDaoTrangs");

            migrationBuilder.DropTable(
                name: "DaoTrangs");

            migrationBuilder.DropTable(
                name: "PhatTus");

            migrationBuilder.DropTable(
                name: "Chuas");

            migrationBuilder.DropTable(
                name: "KieuThanhViens");
        }
    }
}
