﻿using Inter.Entities;
using Inter.IServices;
using Inter.ResponseDTO;

namespace Inter.Services
{
    public class KieuThanhVienService : IKieuThanhVien
    {
        private readonly AppDbContext appDbContext;
        public KieuThanhVienService()
        {
            appDbContext = new AppDbContext();
        }
        public List<KieuThanhVienResponseToFilterDTO> GetAllToFilter()
        {
            return appDbContext.KieuThanhViens.Select(KieuThanhVienResponseToFilterDTO.Mapping).ToList();
        }
    }
}
