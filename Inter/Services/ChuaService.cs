﻿using Inter.Entities;
using Inter.IServices;
using Inter.ResponseDTO;

namespace Inter.Services
{
    public class ChuaService : IChua
    {
        private readonly AppDbContext appDbContext;

        public ChuaService()
        {
            appDbContext = new AppDbContext();
        }

        public List<ChuaResponseToFilterDTO> GetAllToFilter()
        {
            return appDbContext.Chuas.Select(ChuaResponseToFilterDTO.Mapping).ToList();
        }
    }
}
