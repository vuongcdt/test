﻿using Inter.Entities;
using Inter.ExcepptionHandllers;
using Inter.IServices;
using Inter.RequestDTO;
using Inter.ResponseDTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace Inter.Services
{
    public class PhatTuService : IPhatTu
    {
        private readonly AppDbContext appDbContext;

        public PhatTuService()
        {
            appDbContext = new AppDbContext();
        }

        public ResponseMessage Create(PhatTu phatTu)
        {
            addChangePhatTu(phatTu);
            return new ResponseMessage() { Message = "Thêm thành công", StatusCode = 201 };
        }

        public ResponseMessage Update(PhatTu phatTu)
        {
            addChangePhatTu(phatTu);
            return new ResponseMessage() { Message = "Sửa thành công", StatusCode = 200 };
        }

        public ResponseMessage Delete(int id)
        {
            using (var trans = appDbContext.Database.BeginTransaction())
            {

                IQueryable<PhatTu> phatTuDb = appDbContext.PhatTus.Where(e => e.Id == id);
                //if (phatTuDb == null) throw new Exception("phatTu Id not exists !");

                if (phatTuDb.IsNullOrEmpty()) throw new NotFoundException("phatTu Id not exists !");

                IQueryable<PhatTuDaoTrang> phatTuDaoTrangDbList = appDbContext.PhatTuDaoTrangs.Where(e => e.PhatTuId == id);
                IQueryable<DonDangKy> donDangKyDbList = appDbContext.DonDangKys.Where(e => e.PhatTuId == id);

                appDbContext.RemoveRange(phatTuDaoTrangDbList);
                appDbContext.RemoveRange(donDangKyDbList);
                appDbContext.Remove(phatTuDb.First());
                appDbContext.SaveChanges();

                trans.Commit();
                return new ResponseMessage() { Message = "Xóa thành công", StatusCode = 200 };
            }
        }

        public PhatTuServiceDTO GetAll(PhatTuRequestParams phatTuParams)
        {

            //IQueryable<PhatTu> queryPhatTusPagination;

            //List<int> listChuaId = phatTuParams.ChuaFilter.IsNullOrEmpty()
            //    ? new List<int>()
            //    : phatTuParams.ChuaFilter.Split(',').Select(i => int.Parse(i)).ToList();

            //IQueryable<PhatTu> queryPhatTus = appDbContext.PhatTus
            //    .Include(e => e.KieuThanhVien)
            //    .Include(e => e.Chua)
            //    .Where
            //    (
            //        phatTu => phatTuParams.Search.IsNullOrEmpty() ? true : phatTu.PhapDanhUnsigned.Contains(MyUtils.LocDau(phatTuParams.Search))
            //        && (phatTuParams.ChuaFilter.IsNullOrEmpty() ? true : listChuaId.Contains(phatTu.ChuaId))
            //    );

            //if (phatTuParams.SortOrder == "asc")
            //{
            //    queryPhatTusPagination = queryPhatTus
            //       .OrderBy<PhatTu, dynamic>(phatTu =>
            //           phatTuParams.SortBy == "NgaySinh" ? phatTu.NgaySinh :
            //           phatTuParams.SortBy == "NgayXuatGia" ? phatTu.NgayXuatGia :
            //           phatTuParams.SortBy == "NgayHoanTuc" ? phatTu.NgayHoanTuc :
            //           phatTuParams.SortBy == "PhapDanh" ? phatTu.PhapDanh :
            //           phatTu.NgayCapNhat
            //           )
            //       .Skip(phatTuParams.Skip())
            //       .Take(phatTuParams.Size);
            //}
            //else
            //{
            //    queryPhatTusPagination = queryPhatTus
            //       .OrderByDescending<PhatTu, dynamic>(phatTu =>
            //           phatTuParams.SortBy == "NgaySinh" ? phatTu.NgaySinh :
            //           phatTuParams.SortBy == "NgayXuatGia" ? phatTu.NgayXuatGia :
            //           phatTuParams.SortBy == "NgayHoanTuc" ? phatTu.NgayHoanTuc :
            //           phatTuParams.SortBy == "PhapDanh" ? phatTu.PhapDanh :
            //           phatTu.NgayCapNhat
            //           )
            //       .Skip(phatTuParams.Skip())
            //       .Take(phatTuParams.Size);
            //}

            IEnumerable<PhatTu> phatTuList;
            if (phatTuParams.SortOrder == "desc")
            {
                phatTuList = appDbContext.PhatTus
                    .Include(e => e.KieuThanhVien)
                    .Include(e => e.Chua)
                    .Where(phatTuParams.Filter)
                    .OrderByDescending(phatTuParams.Sort)
                    .Skip(phatTuParams.Skip())
                    .Take(phatTuParams.Size);
            }
            else
            {
                phatTuList = appDbContext.PhatTus
                    .Include(e => e.KieuThanhVien)
                    .Include(e => e.Chua)
                    .Where(phatTuParams.Filter)
                    .OrderBy(phatTuParams.Sort)
                    .Skip(phatTuParams.Skip())
                    .Take(phatTuParams.Size);
            }

            int totalItems = appDbContext.PhatTus.Where(phatTuParams.Filter).Count();
            //int totalItems = queryPhatTus.Count();
            double totalPage = Math.Ceiling(((double)totalItems / phatTuParams.Size));

            return new PhatTuServiceDTO
            {
                PhatTuList = phatTuList.Select(PhatTuResponseDTO.Mapping).ToList(),
                //PhatTuList = queryPhatTusPagination.Select(PhatTuResponseDTO.Mapping).ToList(),
                TotalItem = totalItems,
                TotalPage = totalPage
            };
        }


        private PhatTu addChangePhatTu(PhatTu phatTu)
        {
            if (phatTu.Id > 0)
            {
                IQueryable<PhatTu> phatTuDb = appDbContext.PhatTus.AsNoTracking().Where(e => e.Id == phatTu.Id);
                if (phatTuDb.IsNullOrEmpty()) throw new Exception("phatTu Id not exists !");
            }

            if (phatTu.ChuaId == 0 && phatTu.KieuThanhVienId == 0) return phatTu;

            if (phatTu.KieuThanhVienId > 0)
            {
                IQueryable<KieuThanhVien> kieuThanhVienDb = appDbContext.KieuThanhViens.Where(e => e.Id == phatTu.KieuThanhVienId);
                if (kieuThanhVienDb.IsNullOrEmpty()) throw new Exception("KieuThanhVienId not exists !");
            }

            if (phatTu.ChuaId > 0)
            {
                IQueryable<Chua> chuaDb = appDbContext.Chuas.Where(e => e.Id == phatTu.ChuaId);
                if (chuaDb.IsNullOrEmpty()) throw new Exception("ChuaId not exists !");
            }

            appDbContext.PhatTus.Update(phatTu);
            appDbContext.SaveChanges(true);

            return phatTu;
        }

    }
}
