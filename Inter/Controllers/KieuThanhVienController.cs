﻿using Inter.IServices;
using Inter.ResponseDTO;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Inter.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class KieuThanhVienController : ControllerBase
    {

        private readonly IKieuThanhVien kieuThanhVienService;

        public KieuThanhVienController(IKieuThanhVien _kieuThanhVienService)
        {
            this.kieuThanhVienService = _kieuThanhVienService;
        }



        // GET: api/<KieuThanhVienController>
        [HttpGet("filter")]
        public List<KieuThanhVienResponseToFilterDTO> GetAllToFilter()
        {
            return kieuThanhVienService.GetAllToFilter();
        }


    }
}
